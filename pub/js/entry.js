import {Game} from "./Game.js"
import {Prop} from "./engine/prop.js";

export const game = new Game();

let inputA = false;
let inputD = false;
let inputJ = false;
function main() {
    game.tick();
    game.player.move(inputA, inputD, inputJ);
}

function initialize() {
    buildTerrain();
}

function buildTerrain() {
    let canvas = document.getElementById("myCanvas");
    for (let i = 0; i < canvas.width / 32; i++) {
        game.registerForegroundProp(new Prop(i * 32, canvas.height - 32, 32, 32, "img/block.png"));
    }
    for (let i = 0; i < 10; i++) {
        // game.registerForegroundProp(new Prop(i * 32, canvas.height - 32, 32, 32, "img/block.png"));
        game.registerForegroundProp(new Prop(Math.floor((Math.random() * (canvas.height / 32))) * 32,
            Math.floor(Math.random() * (canvas.width / 32)) * 32, 32, 32, "img/block.png"));
    }
    // new Block(Math.floor((Math.random() * (canvas.width / 32))) * 32, Math.floor(Math.random() * (canvas.height / 32)) * 32);
}

initialize();
setInterval(main, 20);

document.addEventListener("keydown", function (event) {
    let player = game.player;
    if (event.defaultPrevented) {
        return;
    }

    switch (event.key) {
        case "a" :
            inputA = true;
            // player.dX = -5;
            break;
        case "d" :
            inputD = true;
            // player.dX = 5;
            break;
        case " " : //space to jump!
            inputJ = true;
            // player.jump();
            break;
        case "ArrowLeft":
            player.fire(Direction.LEFT)
            break;
        case "ArrowRight":
            player.fire(Direction.RIGHT);
            break;
        case "ArrowUp":
            player.fire(Direction.UP);
            break;
        case "ArrowDown":
            player.fire(Direction.DOWN);
            break;
    }


})

document.addEventListener("keyup", function (event) {
    let player = game.player;
    if (event.defaultPrevented) {
        return;
    }

    switch (event.key) {
        case "a" :
            inputA = false;
            // player.dX = 0;
            break;
        case "d" :
            inputD = false;
            // player.dX = 0;
            break;
        case " ":
            inputJ = false;
    }
})

export const Direction = {
    UP : "up",
    DOWN : "down",
    LEFT : "left",
    RIGHT : "right"
}