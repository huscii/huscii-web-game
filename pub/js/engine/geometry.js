/**
 * All rendered game objects both static and dynamic
 */
export class Geometry {

    /**
     *
     * @param {number} x coordinate
     * @param {number} y coordinate
     * @param {number} width
     * @param {number} height
     * @param {boolean} canCollide Can this collide with other objects?
     * @param {?string} sprite
     */
    constructor(x, y, width, height, sprite = null, canCollide = true) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.canCollide = canCollide;
        if (sprite) {
            this.sprite = new Image();
            this.sprite.src = sprite;
        } else {
            this.sprite = null;
        }
    }

    draw(ctx) {
        ctx.beginPath();
        if (!this.sprite) {
            ctx.rect(this.x, this.y, this.width, this.height);
            ctx.fillStyle = "#000000";
            ctx.fill();
        } else {
            ctx.drawImage(this.sprite, this.x, this.y, this.width, this.height);
        }

        ctx.closePath();
    }

    getBoundingPoints() {
        let points = [];
        points.x1 = this.x;
        points.x2 = this.x + this.width;
        points.y1 = this.y;
        points.y2 = this.y + this.height;
        return points;
    }
}