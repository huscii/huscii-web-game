class Sprite {

    /**
     *
     * @param {string} spriteSheet Path to sprite.
     * @param {number} width Width of sprite.
     * @param {number} height  Height of sprite.
     * @param {number} offset Number of extra padding pixels between sprites in the sheet.
     * @param {Map} animations Map of (string key, (x,y, frames) value) where x,y is starting
     * frame and frames is num of frames. Leave empty for no animation. All frames of an animation
     * must exist in the same row of a sprite sheet and all frames must have the same size.
     * @param {string} defaultAnimation The default animation to play. Can be overridden by the
     * setDefaultAnimation method.
     * @param {number} animationDelay Delay in ticks before rendering the next frame of an animation.
     *
     * If no animation is specified then the frame at 0,0 will be rendered by default.
     */
    constructor(spriteSheet, width, height, offset, animations = null, defaultAnimation, animationDelay) {
        this.sheet = new Image()
        this.sheet.src = spriteSheet;
        this.width = width;
        this.height = height;
        this.offset = offset;
        this.animations = animations;
        this.setDefaultAnimation(defaultAnimation);
        this.animationDelay = animationDelay;

        const {x, y, frames} = this.animations.get(defaultAnimation);
        this.x = x;
        this.y = y;
        this.frame = 1;
        this.maxFrames = frames;

    }

    /**
     * Sets the default animation for this sprite using the animation key.
     * If the key is not present throws an error.
     * @param key Key of animation in this sprites animation map.
     */
    setDefaultAnimation(key) {
        if (this.animations.has(key)) {
            this.defaultAnimation = key;
        } else {
            throw new Error("Specified animation does not exist.");
        }
    }


}