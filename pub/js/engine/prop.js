import {Geometry} from "./geometry.js";

/**
 * Static game objects. Anything that will never change.
 */
export class Prop extends Geometry {
    /**
     *
     * @param {number} x coordinate
     * @param {number} y coordinate
     * @param {number} width
     * @param {number} height
     * @param {boolean} canCollide Can this collide with other objects?
     * @param {?string} sprite
     */
    constructor(x, y, width, height, sprite = null, canCollide = true) {
        super(x, y, width, height, sprite, canCollide);

    }
}