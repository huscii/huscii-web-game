import {Geometry} from "./geometry.js";

/**
 * Dynamic game objects. Things that will move or change.
 */
export class Entity extends Geometry {
    constructor(x, y, width, height, sprite = null, canCollide = true) {
        super(x, y, width, height, sprite, canCollide)
        /** @type {number} x velocity*/
        this.dX = 0;
        /** @type {number} y velocity*/
        this.dY = 0;
        this.alive = true;
    }

    /**
     *
     * @param {HTMLCanvasElement} canvas
     * @param {Geometry[]} geometries
     */
    update(canvas, geometries) {
        let newX = this.x + this.dX;
        let newY = this.y + this.dY;
        //OOB Check
        if (newX < 0) {
            this.x = 0;
        } else if (newX > canvas.width - this.width) {
            this.x = canvas.width - this.width;
        } else {
            this.x = newX;
        }

        if (newY < 0) {
            this.y = 0;
        } else if (newY > canvas.height - this.height) {
            this.y = canvas.height - this.height;
        } else {
            this.y = newY;
        }

        //Collisions check
        let collisions = this.collide(geometries)
        let hit = [];
        if (collisions.up) {
            if (!hit.includes(collisions.up)) {
                this.y = collisions.up.y + collisions.up.height;
                hit.push(collisions.up)
                this.dY = 0;
            }
        }
        if (collisions.down) {
            if (!hit.includes(collisions.down)) {
                this.y = collisions.down.y - this.height - 1;
                hit.push(collisions.down)
                this.dY = 0;
            }
        }
        if (collisions.left) {
            if (!hit.includes(collisions.left)) {
                this.x = collisions.left.x + collisions.left.width;
                hit.push(collisions.left)
            }
        }
        if (collisions.right) {
            if (!hit.includes(collisions.right)) {
                this.x = collisions.right.x - this.width;
                hit.push(collisions.right)
            }
        }

        this.collisions = collisions;

    }

    /**
     *
     * @param {Geometry[]} other
     */
    collide(other) {
        /** @type {{left: ?Geometry, right: ?Geometry, up: ?Geometry, down: ?Geometry}} */
        let collision = {left: null, right: null, up: null, down: null};
        for (let geo of other) {
            if (geo === this) continue;
            let o = geo.getBoundingPoints();
            let m = this.getBoundingPoints();

            if ((m.x1 > o.x1 - 1 && m.x1 < o.x2 + 1)
                && ((m.y2 < o.y2 - 1 && m.y2 > o.y1 + 1)
                || (m.y1 < o.y2 + 1 && m.y1 > o.y1 - 1))) {
                collision.left = geo
                console.log("Collide left")
            }
            if ((m.x2 > o.x1 && m.x2 < o.x2)
                && ((m.y2 < o.y2 + 1 && m.y2 > o.y1 - 1)
                || (m.y1 < o.y2 + 1 && m.y1 > o.y1 - 1))) {
                collision.right = geo
                console.log("Collide right")
            }
            if ((m.y1 > o.y1 && m.y1 < o.y2) && ((m.x1 > o.x1 && m.x1 < o.x2) || (m.x2 > o.x1 && m.x2 < o.x2))) {
                collision.up = geo
                console.log("Collide up")
            }
            if ((m.y2 > o.y1 && m.y2 < o.y2) && ((m.x1 > o.x1 && m.x1 < o.x2) || (m.x2 > o.x1 && m.x2 < o.x2))) {
                collision.down = geo
                console.log("Collide down")
            }

        }
        return collision;
    }

}