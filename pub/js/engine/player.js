import {Entity} from "./entity.js";
import {Projectile} from "./projectile.js"
import {Direction} from "../entry.js";
import {game} from "../entry.js";

export class Player extends Entity{
    constructor(canvas) {
        let size = 16;
        let playerSprite = "img/char_sprites.png"
        super(canvas.width / 2, canvas.height - size - 32, size, size, playerSprite);
        this.airborne = false;
        // this.movL = false;
        // this.movR = false;
    }

    move(inputL, inputR, inputJ){
        // this.movL = inputL;
        // this.movR = inputR;

        if(inputL) this.dX = -5;
        if(inputR) this.dX = 5;
        if(inputJ) this.jump();

        if((inputL && inputR) || (!inputL && !inputR)) this.dX = 0;


    }

    /**
     * Fires a bullet from the player's current position
     * @param {Direction} dir
     */
    fire(dir) {
        let x = this.x;
        let y = this.y;
        //Offset bullet position from player a bit
        switch (dir) {
            case Direction.UP:
                y -= 17;
                break;
            case Direction.DOWN:
                y += 16;
                break;
            case Direction.LEFT:
                x -= 16;
                break
            case Direction.RIGHT:
                x += 16;
                break;
            default :
                console.log("Something fucky happened")
        }

        game.registerEntity(new Projectile(dir, x, y));

    }

    /**
     * Makes the player jump, update methods handles inertia/falling
     */
    jump() {
        if (!this.airborne) {
            this.dY = -10;
        }
    }

    /**
     * Updates position of Player based on movement.
     * Additionally, handles inertia of jumping/falling.
     * @param {HTMLCanvasElement} canvas
     * @param {Geometry[]} geometries Objects to check against for collisions
     */
    update(canvas, geometries){
        super.update(canvas, geometries);
        let collisions = this.collisions;
        if (!collisions.down) {
            this.dY += 2;
        }

    }

    draw(ctx) {
        ctx.drawImage(this.sprite, 17, 153, 16, 16, this.x, this.y, 16, 16);
    }

}