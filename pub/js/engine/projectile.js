import {Entity} from "./entity.js";
import {Direction} from "../entry.js";

/**
 * Objects which move and aren't a player or mob. Think bullet. Causes damage, dies on impact.
 */
export class Projectile extends Entity{
    /**
     * Shoots a bullet!
     * @param {Direction} direction
     * @param x
     * @param y
     */


    constructor(direction, x, y) {

        super(x, y, 16, 16, "img/bullet.png")

        this.frame = 1;
        this.frameDelay = 5;
        this.bulletSprites = new Image();
        this.bulletSprites.src = "img/bullet.png";

        switch (direction) {
            case Direction.UP:
                this.dY = -10;
                break;
            case Direction.DOWN:
                this.dY = 10;
                break;
            case Direction.LEFT:
                this.dX = -10;
                break;
            case Direction.RIGHT:
                this.dX = 10;
                break;
        }

    }

    update(canvas, geometries) {
        let newX = this.x + this.dX;
        let newY = this.y + this.dY;
        if (newX < 0 || newX + this.width > canvas.width || newY < 0 || newY + this.height > canvas.height) {
            this.alive = false;
        }
        super.update(canvas, geometries);
        for (let collided of Object.values(this.collisions)) {
            if (collided) {
                this.alive = false;
                break;
            }
        }
        this.frameDelay--;
        if(this.frameDelay < 1){
            this.frame++;
            this.frameDelay = 5;
            if (this.frame > 3){
                this.frame = 1;
            }
        }
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.drawImage(this.bulletSprites, this.frame * 17 - 17, 0, 17, 17, this.x, this.y, 16, 16);
        ctx.closePath();
    }
}