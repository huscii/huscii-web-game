import {Player} from "./engine/player.js";

/**
 * Managed the state of the game as a whole
 * Background props are drawn in the background.
 * Foreground props & entities will be drawn in the foreground layer. Entities eclipse props.
 */
export class Game {
    constructor() {
        this.canvas = document.getElementById("myCanvas");
        this.bgProp = new Set();
        this.fgProp = new Set();
        this.entities = new Set();
        this.player = new Player(this.canvas);
    }


    /**
     * Updates and draws all items in the game.
     */
    tick() {

        // update
        let allGeo = [...Array.from(this.bgProp),
            ...Array.from(this.fgProp),
            ...Array.from(this.entities),
            this.player
        ];
        let ctx = this.canvas.getContext("2d");
        this.player.update(this.canvas, allGeo);
        for (let ent of this.entities) {
            if (!ent.alive) {
                this.deregister(ent);
                continue;
            }
            ent.update(this.canvas, allGeo);
        }
        //draw
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        for (let layer of [this.bgProp, this.fgProp, this.entities]) {
            for (let obj of layer) {
                obj.draw(ctx);
            }
        }
        this.player.draw(ctx);
    }

    /**
     * Add props to be rendered in the background
     * @param {Prop} prop
     */
    registerBackgroundProp(prop) {
        this.bgProp.add(prop);
    }

    /**
     * Add props to be rendered in the game foreground.
     * @param {Prop} prop
     */
    registerForegroundProp(prop) {
        this.fgProp.add(prop);
    }

    /**
     * Add entities to be rendered in the foreground.
     * Entities will render in front of all props.
     * @param {Entity} entity
     */
    registerEntity(entity) {
        this.entities.add(entity);
    }

    deregister(obj) {
        console.log(`Called deregister with ${obj}`)
        this.bgProp.delete(obj);
        this.fgProp.delete(obj);
        this.entities.delete(obj);
    }
}
